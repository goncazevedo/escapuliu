from PPlay.gameobject import*
from PPlay.gameimage import*
from PPlay.sound import*
from PPlay.window import*
from PPlay.keyboard import*
from PPlay.sprite import*
import random

game = True

while game:
    #Arquivos de som
    """
    mainsound = Sound("sound/trilha.ogg")
    mainsound.play()
    mainsound.sound.set_volume(15/100)
    
    cast = Sound("sound/")
    
    hit = Sound("sound/")
    hit.sound.set_volume(20/100)
    
    abrindo = Sound("sound/")
    morte  = Sound("sound/")
    
    """

    janela = Window(1050,639)
    janela.set_title("EscapuLiu")
    teclado = janela.get_keyboard()
    cursor = janela.get_mouse()


    #Imagens e Sprites
    fundo = GameImage("imagens/cenario.png")

    paradod = Sprite("imagens/peach/peach_stand_right.png")
    paradoe = Sprite("imagens/peach/peach_stand_left.png")
    animacaod = Sprite("imagens/peach/peach_right.png",3)
    animacaoe = Sprite("imagens/peach/peach_left.png",3)
    peach_hit = Sprite("imagens/magia_princesa.png",4)

    animacaoe.set_total_duration(333)
    animacaod.set_total_duration(333)
    peach_hit.set_total_duration(333)
    #mago = Sprite("imagens/mago_1.png")
    #magohit = Sprite("imagens/mago_poder.png")

    #guarda = Sprite("imagens/guard.png")

    #chave = Sprite("imagens/chave.png")
    #potion = Sprite("imagens/potion.png")
    #porta = Sprite("imagens/porta.png")

    #Posicionamento inicial
    #A personagem tem a posição setada dentro do jogo

    #Variáveis de jogo
    key = False
    open = False
    vida = 3
    magia = 0
    pos = 1

    while True:
        
        fundo.draw()
        if(teclado.key_pressed("RIGHT")and not (teclado.key_pressed("LEFT"))):
            animacaod.draw()
            animacaod.update()
            
            pos = 1
        elif pos == 1:
            paradod.x=animacaod.x
            paradod.y=animacaod.y
            paradod.draw()
        if(teclado.key_pressed("LEFT") and not (teclado.key_pressed("RIGHT"))):
            animacaoe.draw()
            animacaoe.update()
            
            pos = -1
        elif pos == -1:
            paradoe.x=animacaoe.x
            paradoe.y=animacaoe.y
            paradoe.draw()
        animacaod.move_key_x(0.9)
        animacaoe.move_key_x(0.9)

        if teclado.key_pressed("SPACE"):
            peach_hit.draw()
            peach_hit.set_position(animacaod.x,animacaod.y)
            while peach_hit.x < 1050:
                peach_hit.move_x(1)
                peach_hit.update()
                




        #Aumento de magia(poção)
        #if (animacaoe.collided(potion) or animacaod.collided(potion)):
        #    magia += 1

        #Tipos de dano
        #if (guarda.collided()):
        #    vida -= 1
        #if (magohit.collided()):
        #    vida = 0

        
        #potion.draw()
        janela.update()
